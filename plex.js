const { Client, keys } = require('roku-client');
const { Mutex } = require('async-mutex');

const kPlexAppId = 13535;

const mutex = new Mutex();

exports.shuffle = () => {
  mutex.runExclusive(async () => {
    const cl = await Client.discover();
    await cl.command()
      .home().wait(5000).send();
    await cl.launch(kPlexAppId);
    await cl.command()
      .wait(15000).down()
      .wait(5000).right()
      .wait(1000).up()
      .wait(1000).select()
      .send();
  });
};

exports.showmusic = () => {
  mutex.runExclusive(async () => {
    const cl = await Client.discover();
    await cl.command()
      .home().wait(5000).send();
    await cl.launch(kPlexAppId);
    await cl.command()
      .wait(15000).down()
      .wait(5000).right()
      .send();
  });
};

exports.showvideos = () => {
  mutex.runExclusive(async () => {
    const cl = await Client.discover();
    await cl.command()
      .home().wait(5000).send();
    await cl.launch(kPlexAppId);
    await cl.command()
      .wait(15000).down()
      .wait(1000).down()
      .wait(5000).right()
      .send();
  });
};
