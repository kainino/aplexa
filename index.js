const express = require('express');
const plex = require('./plex.js');

const app = express();
const kPort = 8014;

app.post('/shuffle', async (request, response) => {
  try {
    response.send('');
    await plex.shuffle();
  } catch (ex) {}
});

app.post('/showmusic', async (request, response) => {
  try {
    response.send('');
    await plex.showmusic();
  } catch (ex) {}
});

app.post('/showvideos', async (request, response) => {
  try {
    response.send('');
    await plex.showvideos();
  } catch (ex) {}
});

app.listen(kPort, (err) => {
  if (err) {
    return console.error(err);
  }
  console.log(`listening on port ${kPort}`);
});
